class Jugador
    def initialize(window)
      @x = 300
      @y = 200
      @window=window
      @image = Gosu::Image.new(window, "paco.png", true)
    end
    def draw()
      @image.draw(@x, @y, 0)
    end
    def update()
        if @window.button_down? Gosu::KB_RIGHT
          @x = @x + 1
        elsif @window.button_down? Gosu::MS_LEFT
          @x = @window.mouse_x
          @y = @window.mouse_y
        end
    end
  end