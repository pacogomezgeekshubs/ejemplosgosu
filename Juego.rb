require 'gosu'
require "./Jugador"

class Juego < Gosu::Window
  def initialize
    super 640, 480, false
    self.caption = "Mi primer juego"
    @jugador = Jugador.new(self)
    @music = Gosu::Song.new("stage.wav")
    @music.play(true)
  end
  def draw
    @jugador.draw
  end
  def update()
    @jugador.update
  end
  def needs_cursor?
    true
  end
end