require "gosu"
require "./Jugador"

class Juego < Gosu::Window
    def initialize
        super 800, 800, false
        self.caption = "Mi primer juego"
        @player = Jugador.new(self)
    end
    def draw
        @player.draw
    end
    
    def update
        if button_down? Gosu::KbRight
            @player.update
        end
    end
end