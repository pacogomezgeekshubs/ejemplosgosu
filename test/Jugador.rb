require "gosu"

class Jugador

    def initialize(window)
        @x = 300
        @y = 200
        @image = Gosu::Image.new(window, "paco.png", true)
    end
    def draw()
        @image.draw(@x, @y, 0)
    end
    def update()
        @x = @x + 1
    end
end